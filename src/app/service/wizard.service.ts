import { Injectable } from '@angular/core';
import { House, Wizard } from '../models/Wizard';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HogwartsApi } from './HogwartsApi';

@Injectable({
  providedIn: 'root'
})
export class WizardService {
  wizards: Wizard[] = [];

  constructor(
    private httpClient: HttpClient
  ) { }

  getWizards(): Observable<Wizard[]> {
    return this.httpClient.get<Wizard[]>(HogwartsApi.BASE_URL);
  } 

  getWizardsByHouse(house: House): Observable<Wizard[]> {
    return this.httpClient.get<Wizard[]>(HogwartsApi.BY_HOUSE_URL + house)
  }
}
