export abstract class HogwartsApi {
    static readonly BASE_URL: string = "http://hp-api.herokuapp.com/api/characters";
    static readonly STUDENTS_URL: string = `${HogwartsApi.BASE_URL}/students`;
    static readonly STAFF_URL: string = `${HogwartsApi.BASE_URL}/staff`;
    static readonly BY_HOUSE_URL: string = `${HogwartsApi.BASE_URL}/house/`;
}