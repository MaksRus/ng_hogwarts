import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http"

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WizardArchiveComponent } from './components/wizard-archive/wizard-archive.component';
import { WizardCardComponent } from './components/wizard-card/wizard-card.component';

@NgModule({
  declarations: [
    AppComponent,
    WizardArchiveComponent,
    WizardCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
