import { Wand } from "./Wand";

export type House = "Gryffindor" | "Slytherin" | "Hufflepuff" | "Ravenclaw";

export interface Wizard {
    name: string,
    alternate_name: string[],
    species: string,
    house: House,
    dateOfBirth: string,
    yearOfBirth: string,
    wizard: boolean,
    ancestry: string,
    eyeColour: string,
    hairColour: string,
    wand: Wand,
    patronus: string,
    hogwartsStudent: boolean,
    hogwartsStaff: boolean,
    actor: string,
    alternate_actors: string[],
    alive: boolean,
    image: string,
}