export interface Wand {
    wood: string,
    core: string,
    length: string,
}