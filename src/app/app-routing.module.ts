import { WizardArchiveComponent } from './components/wizard-archive/wizard-archive.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", component: WizardArchiveComponent },
  { path: "house/:house", component: WizardArchiveComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
