import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardArchiveComponent } from './wizard-archive.component';

describe('WizardArchiveComponent', () => {
  let component: WizardArchiveComponent;
  let fixture: ComponentFixture<WizardArchiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WizardArchiveComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WizardArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
