import { House } from './../../models/Wizard';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Wizard } from 'src/app/models/Wizard';
import { WizardService } from 'src/app/service/wizard.service';

@Component({
  selector: 'app-wizard-archive',
  templateUrl: './wizard-archive.component.html',
  styleUrls: ['./wizard-archive.component.scss']
})
export class WizardArchiveComponent implements OnInit {
  wizards: Wizard[] = [];
  house: House | null = null;

  constructor(
    private wizardService: WizardService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      // On récupère la maison dans l'url au cas où
      // l'on se trouve sur la route /house/:house
      this.house = params['house'];

      //Si on a une maison dans l'URL, on cherche "par maison"
      // en fournissant à notre service le nom de la maison
      if (this.house != null) {
        this.wizardService.getWizardsByHouse(this.house).subscribe(wizards => {
          this.wizards = wizards;
        });

        //Sinon, on demande simplement l'intégralité des sorciers
      } else {
        this.wizardService.getWizards().subscribe(wizards => {
          this.wizards = wizards;
        });
      }
    })
  }

}
