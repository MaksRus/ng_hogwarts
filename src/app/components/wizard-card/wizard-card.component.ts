import { Component, OnInit, Input } from '@angular/core';
import { Wizard } from 'src/app/models/Wizard';

@Component({
  selector: 'app-wizard-card',
  templateUrl: './wizard-card.component.html',
  styleUrls: ['./wizard-card.component.scss']
})
export class WizardCardComponent implements OnInit {
  @Input() wizard: Wizard | undefined;

  constructor() { }

  ngOnInit(): void {
  }
}
